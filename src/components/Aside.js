import React from 'react';



function Header(){
  return(
    <aside className="aside-menu">
    <ul className="nav nav-tabs" role="tablist">
       <li className="nav-item">
         <a className="nav-link active" data-toggle="tab" href="#tab1" role="tab">
           Tab 1
         </a>
       </li>
       <li className="nav-item">
         <a className="nav-link" data-toggle="tab" href="#tab2" role="tab">
           Tab 2
         </a>
       </li>
       <li className="nav-item">
         <a className="nav-link" data-toggle="tab" href="#tab3" role="tab">
           Tab 3
         </a>
       </li>
     </ul>

     <div className="tab-content">
       <div className="tab-pane p-3 active" id="tab1" role="tabpanel">
         Tab 1 Content
       </div>
       <div className="tab-pane p-3" id="tab2" role="tabpanel">
         Tab 2 Content
       </div>
       <div className="tab-pane p-3" id="tab3" role="tabpanel">
         Tab 3 Content
       </div>
     </div>

    </aside>
);
}

export default Header;

import React,{Component} from 'react';
import
{Link,
Switch,
Route,
Redirect,
useParams}
from 'react-router-dom';

import Article from "./Article";
import Video from "./Video";
import Lessons from "./Lessons";
class Course extends Component{
  constructor(props){
    super(props);

    this.state={
      lessons:[
        {
          course_name:"chapter1",
          id:"ch1"
        },
        {
          course_name:"chapter21",
          id:"ch21"
        }
      ]
    }

  }



  render(){

     let url=this.state.lessons.map((cs)=>{
        return <Link to={`${this.props.match.url}/${cs.id}`} key={cs.id} className="list-group-item list-group-item-action">{cs.course_name }</Link>
     });


     return(
       <main className="main">
         <div className="container-fluid ">
            <div className="row ">
              <div className="col-8">
              <Switch>
                  <Route exact path={this.props.match.path}>
                    <Redirect to={`${this.props.match.url}/${this.state.lessons[0].id}`} />

                  </Route>
                  <Route path={`${this.props.match.path}/:lessonId`}>
                    <Lesson />
                  </Route>
                </Switch>
              </div>

              <div classname="col-4">
                <div className="card shadow p-3 mb-5 bg-white rounded" style={{width:400}}>
                    <div className="card-header">Lessons</div>
                        <div className="card-body">
                             <div className="list-group">
                                {url}
                              </div>
                        </div>
                  </div>
              </div>


            </div>
         </div>
       </main>

     );
  }
}


class Lesson extends Component {
  constructor(props) {
    super(props);
    this.state={
    };
  }

  componentDidMount(){
    this.setState({
      type:"video",
      content:"https://www.youtube.com/embed/zpOULjyy-n8?rel=0",
      title:"Hello World",
    });
  }

  render(){
    if(this.state.type=="article")
      return <Article content={this.state.content} title={this.state.title}/>
    else if (this.state.type=="video") {
      return <Video content={this.state.content} title={this.state.title}/>
    }
    else
      return "Sorry we have not found Anything";
  }



}
export default Course;

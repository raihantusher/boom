import React,{Component} from 'react';
import Video from './Video';
import Courses from './Courses';

class Main extends Component{

  constructor(props){
    super(props);


  }

  render(){
        return(
          <main className="main">
          <div className="container-fluid">
            <div className="card">
                <div className="card-header"> <i className="fa fa-align-justify"></i> {this.props.title}</div>
                  <div className="card-body justify-content-center">
                      <Courses courses={this.state.courses}/>
                  </div>
                </div>
            </div>
          </main>
      );
    }
}
export default Main;

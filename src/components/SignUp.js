import React,{Component} from 'react';

import {
  Link
} from 'react-router-dom';

class SignUp extends React.Component{
  render(){
    return(
      <div className="container mt-5">
        <div className="row justify-content-center">
            <div className="col-6">

            <div className="card">
                <div className="card-header text-center"><strong>signup</strong> Form</div>
                <div className="card-body">
                  <form className="form-horizontal">
                    <div className="form-group row">
                      <label className="col-md-3 col-form-label" for="fullname">Full Name</label>
                      <div className="col-md-9">
                        <input className="form-control"  type="text" />
                          <span className="help-block">Please enter your full name</span>
                        </div>
                      </div>


                      <div className="form-group row">
                        <label className="col-md-3 col-form-label" for="hf-email">Email</label>
                        <div className="col-md-9">
                          <input className="form-control"  type="email" />
                            <span className="help-block">Please enter your email</span>
                          </div>
                        </div>




                <div className="form-group row">
                  <label className="col-md-3 col-form-label" for="hf-password">Password</label>
                  <div className="col-md-9">
                    <input className="form-control"  type="password" />
                    <span className="help-block">Please enter your password</span>
                  </div>
                </div>



                <div className="d-flex">
                    <Link className="mr-auto mt-auto" to="/login" >LogIn</Link>
                  <button className="btn btn-lg btn-primary" type="submit">
                  <i className="fa fa-dot-circle-o"></i> Login</button>
                </div>
            </form>


                </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}


export default SignUp;

import React from 'react';


function Article(props){

  return (
    <div className="card">
       <div className="card-header">{props.title}</div>
         <div className="card-body"  dangerouslySetInnerHTML={{__html:props.content}}>
       </div>
    </div>
  );
}

export default Article;

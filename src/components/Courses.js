import React,{Component} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  withRouter
} from "react-router-dom";
import axios from 'axios';


import Welcome from './Welcome';

class Courses extends Component{
  constructor(props){
    super(props);

    this.state={
      courses:[],

    };

  }

  componentDidMount(){

    axios.get('http://localhost/el/courses.php', {
            params: {

            }
            })
            .then( (response)=> {
                  console.log(response.data);
                  this.setState({courses:response.data});
            })
            .catch((error)=> {
                  console.log(error);
            })
            .finally( ()=> {
          
          });
  }
  render(){

    const length =this.state.courses.length;

    if(length>0){
      const cards=this.state.courses.map((item)=>

      <p><Link to={'/course/'+item.id} key={item.id}>{item.title}</Link></p>

       );
    return (

        <main className="main">
        <div className="container-fluid">
             <Welcome username="Raihan Tusher"/>
          <div className="card">
              <div className="card-header"> <i className="fa fa-align-justify"></i> {this.props.title}</div>
                <div className="card-body justify-content-center">
                    <div>{cards}</div>
                </div>
              </div>
          </div>
        </main>
    );
   }
   else{
     return (<p>Hello world</p>);
   }

  }
}



export default withRouter(Courses);

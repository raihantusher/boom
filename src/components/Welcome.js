import React from 'react';

const Welcome=({username})=>{
  return (

          <div className="col">
                  <div className="card">
                      <div className="card-body">
                        <h1>Welcome, {username}</h1>
                      </div>
                  </div>
            </div>
    
);
}

export default Welcome;

import React,{Component}from 'react';

class Video extends Component{
  constructor(props) {
    super(props);
  }


render(){
  return(
    <div className="card shadow p-3 mb-5 bg-white rounded">
      <div className="card-header">{this.props.title}</div>
        <div className="card-body  mx-auto">
            <div className="plyr__video-embed" id="player">
                    <iframe
                        src={this.props.content}
                        allowFullScreen

                        allow="autoplay" width="420" height="315"
                    ></iframe>
                    </div>
            </div>
    </div>
  )
}

}
export default Video;

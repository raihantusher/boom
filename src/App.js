import React from 'react';
import './index.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";

import Header from './components/Header';
import Footer from './components/Footer';
import Sidebar from './components/Sidebar';

import Aside from './components/Aside';
import Course from './components/Course';

import Courses from './components/Courses';
import Welcome from './components/Welcome';


import Login from './components/Login';
import SignUp from './components/SignUp';

import CourseManager from './components/CourseManager'
function App() {
  return (
    <Router>

       <div className="App">
         <Header login="true"/>

         <div className="app-body">
           <Sidebar />

          <Switch>
            <Route exact path="/">

                <Courses title="video"/>
            </Route>
            <Route path="/about">

            </Route>
            <Route path="/course/:id" component={Course} />

           <Route path="/login" component={Login}/>
              <Route path="/signup" component={SignUp}/>
              <Route path="/cm" component={CourseManager}/>
          </Switch>





            <Footer/>
        </div>
        </div>

      </Router>
  );
}



export default App;
